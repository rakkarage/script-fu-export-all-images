(define (script-fu-scaleBorderExportAll)
	(let* ((i (car (gimp-image-list))) (image))
		(while (> i 0)
			(set! image (vector-ref (cadr (gimp-image-list)) (- i 1)))
			(gimp-image-scale image 2560 2560)
			(gimp-image-resize image 2800 2800 120 120)
			(gimp-layer-resize-to-image-size (car (gimp-image-get-active-layer image)))
			(file-png-save-defaults RUN-NONINTERACTIVE image
				(car (gimp-image-get-active-layer image))
				(car (gimp-image-get-filename image))
				(car (gimp-image-get-filename image)))
			(gimp-image-clean-all image)
			(gimp-displays-flush)
			(set! i (- i 1)))))

(script-fu-register "script-fu-scaleBorderExportAll"
	"<Image>/File/Scale and border and export All"
	"Scale and border and export all images"
	"rakkarage@gmail.com"
	"rakkarage@gmail.com"
	"2018"
	"")
